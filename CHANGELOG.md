# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.1

- patch: Internal maintenance: make new pipe consistent to others.

## 1.0.0

- major: Note: This pipe was forked from https://bitbucket.org/microsoft/azure-web-apps-deploy for future maintenance purposes.

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Switch naming conventions from task to pipes.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Azure Web Apps deployment pipe.

