FROM microsoft/azure-cli:2.0.57

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
